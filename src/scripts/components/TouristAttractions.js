export class TouristAttractions {
    constructor() {
        this.list = [];

        this.selectors();
        this.events();
    }

    selectors() {
        this.form = document.querySelector(".pontos-form");
        this.imageTitle = document.querySelector(".pontos-form-title");
        this.imageDescription = document.querySelector(".pontos-form-description");
        this.imageUpload = document.querySelector(".pontos-form-image");
        this.items = document.querySelector(".pontos-items");
    }

    events() {
        this.form.addEventListener("submit", this.addImageToList.bind(this));
        this.imageUpload.addEventListener("change", this.showPreview.bind(this));
    }

    showPreview(event) {
        if (event.target.files.length > 0) {
            var src = URL.createObjectURL(event.target.files[0]);
            var preview = document.getElementById("image-preview");
            var label = document.querySelector(".pontos-form-image-label");
            preview.src = src;
            preview.style.display = "block";
        }
    }

    addImageToList(event) {
        event.preventDefault();

        const itemImage = event.target["image-preview"].src;
        const itemTitle = event.target["item-title"].value;
        const itemDescription = event.target["item-description"].value;

        if (itemImage && itemTitle && itemDescription != "") {
            const item = {
                image: itemImage,
                title: itemTitle,
                description: itemDescription,
            };

            this.list.push(item);
            this.renderListItems();
            this.resetInputs();
        }
    }

    renderListItems() {
        let itemsStructure = "";

        this.list.forEach(function (item) {
            itemsStructure += `
              <div class="pontos-item">
                      <img src="${item.image}" />
                  <div class="pontos-item-text">
                      <h2 class="pontos-item-title">${item.title}</h2>
                      <p class="pontos-item-description">${item.description}</p>
                  </div>
              </div>
              `;
        });

        this.items.innerHTML = itemsStructure;
    }

    resetInputs() {
        this.imageUpload.src = "";
        this.imageTitle.value = "";
        this.imageDescription.value = "";
        document.getElementById("image-preview").style.transition = "0.2s";
        document.getElementById("image-preview").style.display = "none";
        document.querySelector(".pontos-form-image-label").style.display = "block";
    }
}
